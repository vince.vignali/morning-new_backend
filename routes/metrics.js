let express = require('express');
let router = express.Router();
const client = require('prom-client');

const collectDefaultMetrics = client.collectDefaultMetrics;
collectDefaultMetrics();

router.get('/', async (req, res) => {
    try {
        const metrics = await client.register.metrics();
        res.set('Content-Type', client.register.contentType);
        res.end(metrics);
    } catch (ex) {
        res.status(500).end(ex);
    }
});

module.exports = router;
